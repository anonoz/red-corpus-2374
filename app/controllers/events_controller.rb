class EventsController < ApplicationController
  def index
    @events = Event.where('occur_at > ?', Time.now).order(occur_at: :desc)
  end

  def new
  end
end
