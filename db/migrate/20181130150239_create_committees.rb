class CreateCommittees < ActiveRecord::Migration[5.2]
  def change
    create_table :committees do |t|
      t.references :club
      t.timestamps
    end
  end
end
