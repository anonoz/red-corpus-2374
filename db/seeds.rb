# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
[
  {name: 'IT Society'},
  {name: 'Student Representative Council'},
  {name: 'MMU Alumni Society'},
  {name: 'Game Dev Club'},
  {name: 'R User Group'}
].each do |club_attributes|
  Club.where(club_attributes).first_or_create!
end

[
  {
    club_id: 1,
    title: 'Tech Career Days 2019',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam tenetur praesentium error repudiandae blanditiis rem inventore labore nemo aliquid repellat, dolorum accusantium iusto fuga perferendis accusamus deserunt consequatur animi, deleniti!',
    occur_at: '2019-01-17T09:00:00+08:00'
  },
  {
    club_id: 3,
    title: 'FCI Homecoming',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia sapiente quos soluta, quas? Magnam est temporibus optio, officia fugit quod et eligendi, minus illum neque doloremque quasi, itaque dolores suscipit.',
    occur_at: '2018-12-02T14:30:00+08:00'
  },
  {
    club_id: 5,
    title: 'Meetup Jan 2019',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci eos vel recusandae dolorem debitis dicta ad, fuga tenetur dolores sed eaque aut harum eveniet cum ipsum commodi. Quos, iusto iste.',
    occur_at: '2019-01-02T19:00:00+08:00'
  }
].each do |event_attributes|
  Event.where(event_attributes).first_or_create!
end

Committee.where(email: 'everything@anonoz.com').first_or_create! do |committee|
  committee.club_id = 1 # it society
  committee.password ='hahahaha'
  end

Admin.where(email: 'everything@anonoz.com').first_or_create! do |admin|
  admin.password = 'hahahaha'
end
